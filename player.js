
const yipee = new Audio('itfs-diversity-scroller/assets/439177__javapimp__lexie-yipee.ogg');

const Player = {
  bottom: 20,
  position: 35,
  gravity: 0.9,
  walkingTimer: undefined,
  walkingTimerCount: 0,
  isGoing: false,
  isJumping: false,
  $player: undefined,

  init () {
    this.$player = $('#player')
  },
  
  yipee () {
    let audio1 = new Audio('./assets/juhuu.mp3');
    let audio2 = new Audio('./assets/yipee.mp3');
    let audio3 = new Audio('./assets/weee.mp3');
    soundlist = [audio1, audio2, audio3];
    random = Math.floor(Math.random() * soundlist.length);
    audio = soundlist[random]
    audio.play()
  },

  jump () {
    if (this.isJumping) return
    this.yipee()
    let timerUpId = setInterval(() => {
      if (this.bottom > 250) {
        clearInterval(timerUpId)
        let timerDownId = setInterval(() => {
          if (this.bottom < 20) {
            clearInterval(timerDownId)
            this.isJumping = false
          }
          this.bottom -= 5;
          this.$player.css('bottom', this.bottom + 'px')
        }, 20)
      }
      this.isJumping = true
      this.bottom += 30
      this.bottom = this.bottom * this.gravity
      this.$player.css('bottom', this.bottom + 'px')
    }, 20)
  },

  walk(){
    Controller.checkPosition()
    if (this.isGoing == 'right') {
      this.position += 5
    } else if (this.isGoing == 'left' ){
      this.position -= 5
    }
    this.$player.css('left', this.position + 'px')
  },

  walkRight() {
    if (this.isGoing == 'right') return
    this.isGoing = 'right'
    this.walkingTimerCount += 1
    if (this.walkingTimerCount === 1) {
      this.walkingTimer = setInterval ( () => {this.walk()}, 20 )
    } else {
      this.walkingTimerCount -= 1
    }
    //console.log('walkingTimerCount:', this.walkingTimerCount, 'right')
  },

  walkLeft() {
    if (this.isGoing == 'left') return
    this.isGoing = 'left'
    this.walkingTimerCount += 1
    if (this.walkingTimerCount === 1) {
      this.walkingTimer = setInterval ( () => {this.walk()}, 20 )
    } else {
      this.walkingTimerCount -= 1
    }
    //console.log('walkingTimerCount:', this.walkingTimerCount, 'left')
  },

  stopWalking() {
    clearInterval(this.walkingTimer)
    this.walkingTimerCount -= 1
    if (this.walkingTimerCount < 0) this.walkingTimerCount = 0
    //console.log('walkingTimerCount:', this.walkingTimerCount, 'stop')
    this.isGoing = false
  },

  shiftX(len) {
    this.position += len
    this.$player.css('left', this.position + 'px')
  }

}  
  