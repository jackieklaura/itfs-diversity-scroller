const Controller = {

    currentRoom: undefined,
    atSchool: false,

    init () {
        // we use once: true, so that not two events might be processed
        // simultaneoulsy. the processKeyEvent hast to register the handler
        // again after it has processed the event
        window.addEventListener("keydown", this.processKeyEvent, {once: true})
        window.addEventListener("keyup", this.processKeyEvent, {once: true})
        this.switchRoomTo(SceneIntro)
        //this.switchRoomTo(Scene4)
    },

    switchRoomTo (room) {
        this.currentRoom = room
        this.currentRoom.init()
        Player.init()
    },

    processKeyEvent(e) {
        // TODO: check if the playground has the focus and only
        //   process the event in this case

        e.preventDefault()

        // as this function will be called by an event handler,
        // its context is window and not the Controller.
        // therfore we assign here self to have a neat replacement for this
        let self = Controller

        // if the current room is a static scene, all key handling
        // will be done there
        if ( self.currentRoom.isStaticScene ) {
            self.currentRoom.processKeyEvent(e)
        }

        // room events will be handled here
        else {
            let keyDown = e.type == 'keydown' ? true : false
            if (e.keyCode === 38) {  // UP
                Player.jump()
            } else if (e.keyCode === 37) {  // LEFT
                if (keyDown) Player.walkLeft()
                else Player.stopWalking()
            } else if (e.keyCode === 39) { // RIGHT
                if (keyDown) Player.walkRight()
                else Player.stopWalking()
            } else if (e.keyCode === 69 && self.atSchool) {  // E
                if (keyDown) {
                    console.log('TODO: entering school')
                    self.currentRoom.destruct()
                    self.currentRoom.nextRoom()
                }
            }    
        }

        // now that the event has been processed we allow for another event
        window.addEventListener("keydown", self.processKeyEvent, {once: true})
        window.addEventListener("keyup", self.processKeyEvent, {once: true})
    },

    checkPosition() {
        // check whether to scroll background when we reach the right side
        if (Player.position > 500) {
            // the maximum x position of the character depends on the level
            let maxPos = 800
            if (this.currentRoom === Room2) maxPos = 700
            else if (this.currentRoom === Room3) maxPos = 630

            if (this.currentRoom.x >= -this.currentRoom.x_shift_max) {
                this.currentRoom.shift('left', 5)
                Player.shiftX(-5)
            }
            // if we have reached the end of the level
            else if (Player.isGoing === 'right' && Player.position > maxPos) {
                Player.stopWalking()
                $action = $('<div id="enter-school-action"></div>')
                $action.text('Press E to enter the school')
                $('#playground').append($action)
                this.atSchool = true
            }
        }

        // check whether to scroll background when we reach the left side
        else if (Player.position < 320) {
            if (this.currentRoom.x <= -this.currentRoom.x_shift_min) {
                this.currentRoom.shift('right', 5)
                Player.shiftX(5)
            }
            
            // if we reach the beginning of the level
            else if (Player.isGoing === 'left' && Player.position < 20) {
                Player.stopWalking()
            }
        }
    }

}