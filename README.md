# Tilda - the daily adventures of a girl who dares to be different

Tilda is a very short but really cute game about Tilda and her daily adventures on the way to school.
It was created during the [ITFS](https://www.itfs.de) Game Jam 2021, by Team A(wesome):

* Judith Greiner ([Instagram: @judyinginside](https://www.instagram.com/judyinginside/))
* jackie | Andrea Ida Malkah Klaura ([web: tantemalkah.at](https://tantemalkah.at))
* Katharina Simma ([linkedin: katharinasimma](https://www.linkedin.com/in/katharinasimma/))

The Game Jam's topic was diversity. The topic is near and dear to our hearts. The goal of our game is to highlight that queer kids often experience loneliness and feel like they do not belong. We want to empower queer kids and make them visible by choosing diverse characters. We want to encourage queer kids to become visible themselves and bring diversity and color to their families and friends.<br>
We hope you enjoy the game and it brightens your day!

The current release version is deployed on: https://tilda.tantemalkah.at

The game source code is available under [AGPLv3](https://gitlab.com/jackieklaura/itfs-diversity-scroller/-/blob/master/LICENSE) version.

All artwork and jumping sounds are licensed under a [CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/) license.

Attributions for background music:

* Level 1: [Ofelia's Dream](https://www.bensound.com/royalty-free-music/track/ofelias-dream) by bensound.com , Free License with Attribution
* Level 2: Lost in Dreams by Ahjay Stelino on https://mixkit.co ,  Mixkit Stock Music Free License (Non-commercial website)
* Level 3: [Smile](https://www.bensound.com/royalty-free-music/track/smile) by bensound.com , Free License with Attribution
* Final Scene: [Hey!](https://www.bensound.com/royalty-free-music/track/hey-happy-cheerful) by bensound.com , Free License with Attribution
