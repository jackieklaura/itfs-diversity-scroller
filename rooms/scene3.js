const Scene3 = {
    isStaticScene: true,
    stage: 0,

    init () {
        this.stage = 0

        const $playground = $('#playground')
        $playground.css('background-image', 'none')
        $playground.addClass('scene-3-background')

        const $player = $('<div id="player"></div>')
        $player.css('left', '100px')
        $player.css('bottom', '70px')
        $playground.append($player)

        const $msg1 = $('<div></div>')
        $msg1.addClass('scene-dialog')
        $msg1.append($('<p>Tilda: Sam, look over there a new kid. They look kind of lonely.</p>'))
        $msg1.append($('<p>Sam: We should ask them to join us!</p>'))
        $msg1.append($('<p class="space">(press space to continue)</p>'))
        $playground.append($msg1)
    },

    destruct () {
        $('#playground').empty()
    },

    // this is not called by a window event handler directly, but
    // by the controller's processKeyEvent, therefore this can be used
    processKeyEvent(e) {
        const $playground = $('#playground')
        if (e.type === 'keydown' && e.keyCode === 32) {
            if ( this.stage === 0 ) {
                const $msg2 = $('<div>You just found another friend!</div>')
                $msg2.addClass('scene-dialog')
                $playground.append($msg2)
                const $friend1 = $('<div id="friend2"></div>')
                $friend1.css('right', '35px')
                $friend1.css('bottom', '230px')
                $playground.append($friend1)
                this.stage += 1
            }

            else {
                this.destruct()
                Controller.switchRoomTo(Room3)
            }
        }

    },
}
