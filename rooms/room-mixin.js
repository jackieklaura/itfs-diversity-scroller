const RoomMixin = {
    shift (dir, len) {
        const $playground = $('#playground')
        if (dir == 'right') {
            this.x += len
        } else {
            this.x -= len
        }
        $playground.css('background-position', `left ${this.x}px bottom 0px`)
    }
}