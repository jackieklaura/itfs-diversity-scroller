const Scene2 = {
    isStaticScene: true,
    stage: 0,

    init () {
        this.stage = 0

        const $playground = $('#playground')
        $playground.css('background-image', 'none')
        $playground.addClass('scene-2-background')

        const $player = $('<div id="player"></div>')
        $player.css('left', '100px')
        $player.css('bottom', '70px')
        $playground.append($player)

        const $msg1 = $('<div></div>')
        $msg1.addClass('scene-dialog')
        $msg1.append($('<p>There I am another boring lonely day ...</p>'))
        $msg1.append($('<p>... wait a moment ... who is the kid overthere? ... </p>'))
        $msg1.append($('<p class="space">(press space to continue)</p>'))
        $playground.append($msg1)
    },

    destruct () {
        $('#playground').empty()
    },

    // this is not called by a window event handler directly, but
    // by the controller's processKeyEvent, therefore this can be used
    processKeyEvent(e) {
        const $playground = $('#playground')
        if (e.type === 'keydown' && e.keyCode === 32) {
            if ( this.stage === 0 ) {
                const $msg2 = $('<div>Hey, I\'m Sam! Can I sit here?</div>')
                $msg2.addClass('scene-dialog')
                $playground.append($msg2)
                const $msg3 = $('<div>Wow, I just found a new friend!</div>')
                $msg3.addClass('scene-dialog')
                $playground.append($msg3)
                const $friend1 = $('<div id="friend1"></div>')
                $friend1.css('right', '50px')
                $friend1.css('bottom', '170px')
                $playground.append($friend1)
                this.stage += 1
            }

            else {
                this.destruct()
                Controller.switchRoomTo(Room2)
            }
        }

    },
}
