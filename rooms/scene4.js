const Scene4 = {
    isStaticScene: true,
    stage: 0,
    processKeys: false,
    $playground: undefined,
    music: undefined,

    init () {
        this.music = new Audio('./assets/bensound-hey.mp3');
        this.music.play()
        this.stage = 0

        this.$playground = $('#playground')
        this.$playground.css('background-image', 'none')
        this.$playground.addClass('scene-4-background')

        const $player = $('<div id="player"></div>')
        $player.css('left', '100px')
        $player.css('bottom', '70px')
        this.$playground.append($player)

        const $msg1 = $('<div></div>')
        $msg1.addClass('scene-dialog')
        $msg1.append($('<p>What a wonderful day!</p>'))
        $msg1.append($('<p>Did you just make another friend? Yes!</p>'))
        this.$playground.append($msg1)

        const $friend1 = $('<div id="friend1"></div>')
        $friend1.css('right', '35px')
        $friend1.css('bottom', '230px')
        this.$playground.append($friend1)

        setTimeout(() => {Scene4.stage1()}, 2000)
    },

    destruct () {
      this.music.pause()
        $('#playground').empty()
    },

    stage1 () {
        this.stage = 1

        const $msg = $('<div></div>')
        $msg.addClass('scene-dialog')
        $msg.append($('<p>Your whole gang is with you!</p>'))
        this.$playground.append($msg)

        const $friend2 = $('<div id="friend2"></div>')
        $friend2.css('right', '50px')
        $friend2.css('bottom', '30px')
        this.$playground.append($friend2)

        // TODO: fix this. somehow #friend3 css does not get applied
        const $friend3 = $('<div id="friend3"></div>')
        $friend3.css('right', '150px')
        $friend3.css('bottom', '150px')
        this.$playground.append($friend3)

        setTimeout(() => {Scene4.stage2()}, 2000)
    },

    stage2 () {
        this.stage = 2

        const $msg = $('<div></div>')
        $msg.addClass('scene-dialog')
        $msg.append($('<p>Together you are strong and no one can hurt you!</p>'))
        this.$playground.append($msg)

        setTimeout(() => {Scene4.stage3()}, 2000)
    },

    stage3 () {
        this.stage = 3

        const $msg = $('<div></div>')
        $msg.addClass('scene-dialog')
        $msg.append($('<p>Thanks for playing our little game</p>'))
        $msg.append($('<p class="space">(hit space to go to the credits scene)</p>'))
        this.$playground.append($msg)

        this.processKeys = true
    },

    // this is not called by a window event handler directly, but
    // by the controller's processKeyEvent, therefore this can be used
    processKeyEvent(e) {
        if (this.processKeys) {
            const $playground = $('#playground')
            if (e.type === 'keydown' && e.keyCode === 32) {
                if ( this.stage === 3 ) {
                    // TODO: change to credits scene
                    alert('By the mighty witchcraftry of mother of time!\n\nThis is not yet implemented.')
                }
    
            }
        }
    },
}
