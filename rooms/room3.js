const Room3 = {
    background: 'assets/bg-level3.jpg',
    x: 0,
    x_shift_min: 0,
    x_shift_max: 1820,
    music: undefined,

    init () {
        this.music = new Audio('./assets/bensound-smile.mp3');
        this.music.play()
        this.x = 0
        const $playground = $('#playground')
        $playground.css('background-image', `url("${this.background}")`)
        $playground.css('background-position', `left ${this.x}px bottom 0px`)

        const $player = $('<div id="player"></div>')
        $player.addClass('room3')
        Player.position = 50
        $player.css('left', '50px')
        $player.css('bottom', '20px')
        $playground.append($player)
    },

    destruct () {
      this.music.pause()
        $('#playground').empty()
    },

    nextRoom () {
        Controller.switchRoomTo(Scene4)
    },

    ...RoomMixin
}
