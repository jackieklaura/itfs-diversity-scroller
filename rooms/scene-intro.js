const SceneIntro = {
    isStaticScene: true,
    stage: 0,

    init () {
        const $playground = $('#playground')
        $playground.css('background-image', 'none')
        $playground.addClass('scene-intro-background')

        const $player = $('<div id="player"></div>')
        $player.css('left', '150px')
        $player.css('bottom', '100px')
        $playground.append($player)

        const $welcomeMessage = $('<div></div>')
        $welcomeMessage.addClass('scene-dialog')
        $welcomeMessage.append($('<p>... another grey morning in a grey city ...</p>'))
        $welcomeMessage.append($('<p class="space">(press space to continue)</p>'))
        $playground.append($welcomeMessage)
    },

    destruct () {
        $('#playground').empty()
    },

    // this is not called by a window event handler directly, but
    // by the controller's processKeyEvent, therefore this can be used
    processKeyEvent(e) {
        const $playground = $('#playground')
        if (e.type === 'keydown' && e.keyCode === 32) {
            if ( this.stage === 0 ) {
                const $msg2 = $('<div>I wish I didn\'t have to go to school! The city is sooo grey and people look at me. Also, I have no friends, because there is no one who is like me at school!</div>')
                $msg2.addClass('scene-dialog')
                $playground.append($msg2)
                this.stage += 1
            }

            else if ( this.stage === 1 ) {
                const $msg3 = $('<div>Well, I guess I have to go anyway ... </div>')
                $msg3.addClass('scene-dialog')
                $playground.append($msg3)
                this.stage += 1
            }

            else {
                this.destruct()
                Controller.switchRoomTo(Room1)
            }
        }

    },
}
